package main

import (
	"database/sql"
	"hw3/pkg/stat"
	"log"
)

func main() {
	db, err := sql.Open("sqlite3", "./database/init.db")
	if err != nil {
		log.Fatalf(err.Error())
	}
	log.Println("db has been created")

	if _, err = db.Exec(`
		create table if not exists "stat" (
			name varchar(64) not null,
			result bool default false,
			time int default 0
		);
		
		create table if not exists "urls" (
			id integer primary key autoincrement,
			path varchar(100)
		)
		`,
	); err != nil {
		log.Fatalf("error while initing db: %v", err)
	}

	srv := stat.Server{DB: db}
	srv.Init()
	messages, _ := srv.InitQueue()
	for message := range messages {
		go srv.Handle(message)
	}
}
