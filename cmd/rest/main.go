package main

import (
	"database/sql"
	"github.com/gorilla/mux"
	"hw3/pkg/rest"
	"log"
	"net/http"
)

func main() {
	db, err := sql.Open("sqlite3", "./database/init.db")
	if err != nil {
		log.Fatalf(err.Error())
	}
	log.Println("db has been created")

	if _, err = db.Exec(`
		create table if not exists "users" (
			name  varchar(64) primary key,
			image varchar(500),
			sex   varchar(10),
			email varchar(64) unique
		);
		
		create table if not exists "urls" (
			id integer primary key autoincrement,
			path varchar(100)
		)
		`,
	); err != nil {
		log.Fatalf("error while initing db: %v", err)
	}

	srv := rest.Server{DB: db}

	srv.MQInit()

	router := mux.NewRouter()
	router.HandleFunc("/user", srv.GetUser).Methods(http.MethodGet)
	router.HandleFunc("/user", srv.AddUser).Methods(http.MethodPost)
	router.HandleFunc("/user", srv.EditUser).Methods(http.MethodPut)
	router.HandleFunc("/user", srv.DeleteUser).Methods(http.MethodDelete)
	router.HandleFunc("/user/image", srv.EditImage).Methods(http.MethodPost)
	router.HandleFunc("/users", srv.GetUsers).Methods(http.MethodGet)
	router.HandleFunc("/user/stat", srv.RequestStat).Methods(http.MethodGet)
	router.HandleFunc("/stat", srv.GetStat).Methods(http.MethodGet)

	http.ListenAndServe(":7778", router)
}
