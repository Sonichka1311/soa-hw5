package stat

import (
	"encoding/json"
	"fmt"
	"github.com/jung-kurt/gofpdf"
	"github.com/streadway/amqp"
	"hw3/pkg/datastruct"
	"io/ioutil"
	"log"
	"net/http"
)

func (s *Server) Handle(msg amqp.Delivery) bool {
	var req datastruct.StatReq
	json.Unmarshal(msg.Body, &req)
	log.Println(req)

	userRes, err := http.Get(fmt.Sprintf("http://rest:7778/user?username=%s", req.Username))
	if err != nil {
		log.Println(err)
		return false
	}
	defer userRes.Body.Close()
	data, _ := ioutil.ReadAll(userRes.Body)
	var user datastruct.User
	json.Unmarshal(data, &user)
	log.Println(user)

	q, err := s.DB.Prepare("select count(*) from stat where name=?")
	if err != nil {
		log.Println(err)
		return false
	}
	var total int64
	q.QueryRow(&total)

	q, err = s.DB.Prepare("select count(*) from stat where name=? and result=true")
	if err != nil {
		log.Println(err)
		return false
	}
	var win int64
	q.QueryRow(&win)

	q, err = s.DB.Prepare("select count(*) from stat where name=? and result=false")
	if err != nil {
		log.Println(err)
		return false
	}
	var lose int64
	q.QueryRow(&lose)

	q, err = s.DB.Prepare("select sum(time) from stat where name=?")
	if err != nil {
		log.Println(err)
		return false
	}
	var time int64
	q.QueryRow(&time)

	log.Println(total, win, lose, time)

	path := fmt.Sprintf("../../stat/%d.pdf", req.Id)

	pdf := gofpdf.New("L", "mm", "Letter", "")
	pdf.AddPage()
	pdf.SetFont("Times", "B", 28)
	pdf.Cell(40, 10, "Statistic")
	pdf.Ln(12)

	pdf.SetFont("Times", "", 20)

	pdf.Text(20, 30, "Name:\t" + user.Name)
	pdf.Text(20, 40, "Sex:\t" + user.Sex)
	pdf.Text(20, 50, "Email:\t" + user.Email)

	pdf.Text(20, 60, fmt.Sprintf("Total:\t%d", total))
	pdf.Text(20, 70, fmt.Sprintf("Won:\t%d", win))
	pdf.Text(20, 80, fmt.Sprintf("Lost:\t%d", lose))
	pdf.Text(20, 90, fmt.Sprintf("Time:\t%d", time))

	if len(user.Avatar) > 0 {
		pdf.ImageOptions(fmt.Sprintf("../../images/%s_image.jpg", req.Username), 110, 25, 60, 80, false, gofpdf.ImageOptions{ImageType: "JPG", ReadDpi: true}, 0, "")
	}

	pdf.OutputFileAndClose(path)

	q, err = s.DB.Prepare("update urls set path=? where id=?")
	if err != nil {
		log.Println(err)
		return false
	}
	_, err = q.Exec(path, req.Id)
	if err != nil {
		log.Println(err)
		return false
	}

	msg.Ack(false)
	return true
}