package stat

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/streadway/amqp"
	"log"
	"time"
)

type Server struct {
	DB        *sql.DB
	Connector *amqp.Connection
	Channel   *amqp.Channel
}

func (s *Server) Init() error {
	counter := 0
	for {
		counter++
		var connectionError error
		s.Connector, connectionError = amqp.Dial("amqp://guest:guest@rabbitmq:5672/")
		if connectionError == nil {
			break
		}
		if counter == 10 {
			return connectionError
		}
		log.Printf("Failed to connect to queue: %s. Retrying...", connectionError)
		time.Sleep(time.Second * 2)
	}
	log.Println("Connected to queue.")

	var channelError error
	s.Channel, channelError = s.Connector.Channel()
	if channelError != nil {
		log.Printf("Notifications: Init error: %s\n", channelError.Error())
		return channelError
	}

	exchangeError := s.Channel.ExchangeDeclare("stat", "fanout", true, false, false, false, nil)
	if exchangeError != nil {
		log.Printf("Notifications: Init error: %s\n", exchangeError.Error())
		return exchangeError
	}
	return nil
}

func (s *Server) InitQueue() (<-chan amqp.Delivery, error) {
	queue, declareError := s.Channel.QueueDeclare("stat", false, false, false, false, nil)
	if declareError != nil {
		log.Printf("Failed to create queue: %s", declareError)
		return nil, declareError
	}

	bindError := s.Channel.QueueBind(queue.Name, "#", "stat", false, nil)
	if bindError != nil {
		log.Printf("Failed to bind queue: %s", bindError)
		return nil, bindError
	}

	return s.Channel.Consume(queue.Name, "", false, false, false, false, nil)
}

func (s *Server) Close() {
	_ = s.Channel.Close()
	_ = s.Connector.Close()
}