package consts

type Sex string

const (
	Male   = "male"
	Female = "female"
)
