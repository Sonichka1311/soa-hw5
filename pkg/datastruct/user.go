package datastruct

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type User struct {
	Name   string `json:"name"`
	Avatar []byte `json:"image,omitempty"`
	Sex    string `json:"sex"`
	Email  string `json:"email"`
}

func GetUserFromRequest(r *http.Request) *User {
	defer r.Body.Close()

	data, _ := ioutil.ReadAll(r.Body)
	log.Println(string(data))

	var user User
	if err := json.Unmarshal(data, &user); err != nil {
		log.Println(err)
	}
	return &user
}
