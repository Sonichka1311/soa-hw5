package rest

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func (s *Server) EditImage(w http.ResponseWriter, r *http.Request) {
	img, _, err := r.FormFile("image")
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	data, _ := ioutil.ReadAll(img)

	if r.URL.Query().Get("username") == "" {
		json.NewEncoder(w).Encode(struct{ Status string }{ Status: "error: cgi param username should be provided" })
		return
	}

	path := "../../images/" + r.URL.Query().Get("username")+"_image.jpg"
	f, _ := os.Create(path)
	f.Write(data)
	f.Sync()

	q, err := s.DB.Prepare("update users set image=? where name=?")
	if err != nil {
		log.Printf("err when prepare query in add image: %v", err)
		json.NewEncoder(w).Encode(struct{ Status string }{ Status: "error: "+err.Error() })
		return
	}
	q.Exec(path, r.URL.Query().Get("username"))

	json.NewEncoder(w).Encode(struct{ Status string }{ Status: "ok" })
}
