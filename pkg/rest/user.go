package rest

import (
	"encoding/json"
	"fmt"
	"hw3/pkg/datastruct"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func (s *Server) GetUser(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("username") == "" {
		json.NewEncoder(w).Encode(struct{ Status string }{ Status: "error: cgi param username should be provided" })
		return
	}

	q, err := s.DB.Prepare("select name, image, sex, email from users where name=?")
	if err != nil {
		log.Printf("err when prepare query in get user: %v", err)
		json.NewEncoder(w).Encode(struct{ Status string }{ Status: "error: "+err.Error() })
		return
	}

	user := datastruct.User{}
	var image *string
	res := q.QueryRow(r.URL.Query().Get("username"))
	err = res.Scan(&user.Name, &image, &user.Sex, &user.Email)
	if err != nil {
		log.Println(err.Error())
	}

	if image != nil {
		f, _ := os.Open(*image)
		data, _ := ioutil.ReadAll(f)
		user.Avatar = data
	}

	json.NewEncoder(w).Encode(user)
}

func (s *Server) AddUser(w http.ResponseWriter, r *http.Request) {
	user := datastruct.GetUserFromRequest(r)
	log.Println(user)

	q, err := s.DB.Prepare("insert into users (name, sex, email) values (?, ?, ?)")
	if err != nil {
		log.Printf("err when prepare query in add user: %v", err)
		return
	}
	_, err = q.Exec(user.Name, user.Sex, user.Email)
	if err != nil {
		log.Printf("err when exec query in add user: %v", err)
		return
	}
	json.NewEncoder(w).Encode(user)
}

func (s *Server) EditUser(w http.ResponseWriter, r *http.Request) {
	user := datastruct.GetUserFromRequest(r)

	fields := make([]string, 0, 3)
	values := make([]interface{}, 0, 3)

	if len(user.Sex) > 0 {
		fields = append(fields, "sex=?")
		values = append(values, user.Sex)
	}

	if len(user.Email) > 0 {
		fields = append(fields, "email=?")
		values = append(values, user.Email)
	}

	values = append(values, user.Name)

	q, err := s.DB.Prepare(fmt.Sprintf("update users set %s where name=?", strings.Join(fields, ", ")))
	if err != nil {
		log.Printf("err when prepare query in edit user: %v", err)
		return
	}
	q.Exec(values...)
	json.NewEncoder(w).Encode(user)
}

func (s *Server) DeleteUser(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("username")
	q, err := s.DB.Prepare("delete from users where name=?")
	if err != nil {
		log.Printf("err when prepare query in delete user: %v", err)
		json.NewEncoder(w).Encode(struct{ Status string }{ Status: "error: "+err.Error() })
		return
	}
	q.QueryRow(name)
	json.NewEncoder(w).Encode(struct{ Status string }{ Status: "ok" })
}

func (s *Server) GetUsers(w http.ResponseWriter, r *http.Request) {
	q, err := s.DB.Prepare("select name, image, sex, email from users")
	if err != nil {
		log.Printf("err when prepare query in get users: %v", err)
		json.NewEncoder(w).Encode(struct{ Status string }{ Status: "error: "+err.Error() })
		return
	}

	users := make([]*datastruct.User, 0)

	res, _ := q.Query()
	for res.Next() {
		user := datastruct.User{}
		var image *string
		res.Scan(&user.Name, &image, &user.Sex, &user.Email)

		if image != nil {
			f, _ := os.Open(*image)
			data, _ := ioutil.ReadAll(f)
			user.Avatar = data
		}

		users = append(users, &user)
	}

	json.NewEncoder(w).Encode(users)
}
