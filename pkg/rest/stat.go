package rest

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"hw3/pkg/datastruct"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

func (s *Server) RequestStat(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("username") == "" {
		w.Write([]byte("username should be provided"))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	q, err := s.DB.Prepare("insert into urls (path) values ('')")
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	resp, err := q.Exec()
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	id, _ := resp.LastInsertId()
	req := datastruct.StatReq{
		Id:       id,
		Username: r.URL.Query().Get("username"),
	}

	jsonReq, _ := json.Marshal(req)

	s.Channel.Publish(
		"stat",
		"",
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        jsonReq,
		},
	)

	w.Write([]byte(fmt.Sprintf("your link: http://%s:7778/stat?id=%d", "localhost", id)))
}

func (s *Server) GetStat(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Get("id") == "" {
		w.Write([]byte("id should be provided"))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id, _ := strconv.Atoi(r.URL.Query().Get("id"))

	//q, _ := s.DB.Prepare("select path from urls where id=?")
	//row := q.QueryRow(id)

	//var path string
	//row.Scan(&path)

	//log.Println(path)

	f, _ := os.Open(fmt.Sprintf("../../stat/%d.pdf", id))
	data, _ := ioutil.ReadAll(f)

	if len(data) == 0 {
		data = []byte("statistics in progress")
	}

	w.Write(data)
}
