FROM golang:latest

RUN mkdir /app/
WORKDIR /app/

COPY cmd cmd
COPY pkg pkg
COPY database database
COPY go.mod go.mod
COPY go.sum go.sum

RUN mkdir images
RUN mkdir stat

ENTRYPOINT ["go", "run", "./cmd/main.go"]