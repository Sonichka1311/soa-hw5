module hw3

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/jung-kurt/gofpdf v1.16.2
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/signintech/gopdf v0.9.15
	github.com/streadway/amqp v1.0.0
)
